#pragma once

class IState;

class StateMachine
{
public:
	
	void update(unsigned long microDiff);
	void switchState(IState *newState);

private:
	IState *currentState;
};
