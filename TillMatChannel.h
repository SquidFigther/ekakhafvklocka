#pragma once

#include "Arduino.h"

class TillMatChannel
{
public:

	TillMatChannel(int channel, class TillMatState &tillMat);

	bool isOwner(class Channel *channel);
	void acquire(class Channel *channel);
	bool canAcquire();

	void setTime(int time);
	void setBuzzer(boolean shouldBeOn);
	void setOnStatus(boolean isOn);
	void setDiod(boolean isOn);

private:

	int channel;

	bool isBuzzerOn = false;
	class TillMatState &tillMat;

	static int buzzerCount;
	static class Channel *currentChannel;
};
