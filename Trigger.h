#pragma once

#include "Arduino.h"
#include "Config.h"

class Trigger
{
public:
	Trigger(int pinPiezo1, int pinPot) 
		: pinPiezo1(pinPiezo1)
		, pinPot(pinPot)
	{
		
	}

	void update(unsigned long microDiff)
	{
		isTrigged = false;

		if (isTriggable)
		{
			if (checkIfTrigged())
			{
				isTrigged = true;
				isTriggable = false;
				time = 0;
			}
		}
		else
		{
			time += microDiff;

			if (time > triggersFilterTime)
			{
				isTriggable = true;
			}
		}
	}

	void setReading(boolean isReading)
	{
		this->isReading = isReading;
	}

	boolean Reading()
	{
		return isReading;
	}

	boolean trigged()
	{
		return isTrigged;
	}

private:
	boolean checkIfTrigged()
	{
		return (isReading && (analogRead(pinPiezo1) > analogRead(pinPot)));
	}

	const int pinPiezo1;
	const int pinPot;

	boolean isReading = false;
	boolean isTrigged = false;
	boolean isTriggable = true;

	unsigned long time;
};