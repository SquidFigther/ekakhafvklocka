#pragma once
#include "Arduino.h"

class ChannelLCDPrinter
{
public:

	ChannelLCDPrinter(int channel);
	
	void resetChannel();

	void printLCDTime(class Clock &clock1, class Clock &clock2);
	void printTTime(unsigned long time);
	void printPTime(unsigned int penaltyTime);
	void printFinalTime(unsigned long timeSec, unsigned long timeRest, boolean hadPenalty);

private:

	void printRow(int row, class Clock &clock);

	int channel;
};
