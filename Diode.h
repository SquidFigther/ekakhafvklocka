#pragma once

#include "Arduino.h"
#include "Config.h"

class Diode
{
public:
	Diode(int pin, boolean commonAnode)
		: pin(pin)
		, commonAnode(commonAnode)
	{
	}

	void turnOn()
	{ 
		if (commonAnode)
		{
			digitalWrite(pin, LOW);
		}
		else
		{
			digitalWrite(pin, HIGH);
		}
	}

	void turnOff()
	{
		if (commonAnode)
		{
			digitalWrite(pin, HIGH);
		}
		else
		{
			digitalWrite(pin, LOW);
		}
	}

private:
	const int pin;
	const boolean commonAnode;
};