#pragma once

class IState
{
public:
	virtual void activate() = 0;
	virtual void update(unsigned long microDiff) = 0;
};