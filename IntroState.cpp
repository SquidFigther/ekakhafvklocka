#include "IntroState.h"
#include "ClockState.h"
#include "HardwareManager.h"
#include "StateMachine.h"

extern HardwareManager hardwareManager;
extern StateMachine stateMachine;

extern ClockState clockState;

void IntroState::activate()
{
	BaseState::activate();

	hardwareManager.lcd.setCursor(8, 1);
	hardwareManager.lcd.print("EKAK");
	hardwareManager.lcd.setCursor(5, 2);
	hardwareManager.lcd.print("H" "\xe1" "fvklocka");
	hardwareManager.lcd.setCursor(0, 3);
	hardwareManager.lcd.print("Jock");
	hardwareManager.lcd.setCursor(16, 3);
	hardwareManager.lcd.print("v313");

	hardwareManager.diodeTrigger1.turnOn();
	hardwareManager.diodeTrigger2.turnOn();

	tillMatState.time1 = 13;
	tillMatState.time2 = 37;
	tillMatState.buzzer = false;
	tillMatState.diod1 = true;
	tillMatState.diod2 = true;
	tillMatState.rightDisplayOff = false;
	tillMatState.leftDisplayOff = false;

	hardwareManager.ledControl.setChar(0, 0, '_', false);
	hardwareManager.ledControl.setChar(0, 1, '-', false);
	hardwareManager.ledControl.setChar(0, 2, 'A', false);
	hardwareManager.ledControl.setChar(0, 3, '-', false);
	hardwareManager.ledControl.setChar(0, 4, '_', false);

	//callar s� att det inte kommer ett loop sent
	hardwareManager.tillMat.update(tillMatState);

	timer.start(oneSecondInMicroSeconds * introTimeInSec);
}

void IntroState::updateDisplay()
{
	BaseState::updateDisplay();
}

void IntroState::update(unsigned long microDiff)
{
	timer.update(microDiff);

	if (timer.isDone())
	{
		stateMachine.switchState(&clockState);
	}

	BaseState::update(microDiff);
}
