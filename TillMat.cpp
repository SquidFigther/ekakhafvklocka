#include "TillMat.h"

TillMatState::TillMatState()
{
	time1 = 0;
	time2 = 0;
	diod1 = false;
	diod2 = false;
	buzzer = false;
	rightDisplayOff = true;
	leftDisplayOff = true;
}

void TillMat::update(const TillMatState & updateState)
{
	//vet inte blir fel ibland n�r man inte shiftar ut tom data f�rst
	//shiftOut(pSER, pSRCLK, MSBFIRST, 0);

	digitalWrite(pRCLK, LOW);

	int shiftOutValue1 = updateState.leftDisplayOff ? OFF_LEFT : GetLeftValue(updateState.time1 / 10);
	int shiftOutValue2 = updateState.leftDisplayOff ? OFF_LEFT : GetLeftValue(updateState.time1 % 10);
	int shiftOutValue3 = updateState.rightDisplayOff ? OFF_RIGHT : GetRightValue(updateState.time2 / 10);
	int shiftOutValue4 = updateState.rightDisplayOff ? OFF_RIGHT : GetRightValue(updateState.time2 % 10);

	shiftOutValue1 |= updateState.diod1 ? 1 : 0;
	shiftOutValue2 |= updateState.buzzer ? 1 : 0;
	shiftOutValue4 |= updateState.diod2 ? 2 : 0;

	shiftOut(pSER, pSRCLK, MSBFIRST, shiftOutValue1);
	shiftOut(pSER, pSRCLK, MSBFIRST, shiftOutValue2);
	shiftOut(pSER, pSRCLK, MSBFIRST, shiftOutValue3);
	shiftOut(pSER, pSRCLK, MSBFIRST, shiftOutValue4);

	digitalWrite(pRCLK, HIGH);
}

int TillMat::GetLeftValue(int i)
{
	int clockOut = 0;

	if (i == 0)
		clockOut = 16;
	else if (i == 1)
		clockOut = 188;
	else if (i == 2)
		clockOut = 34;
	else if (i == 3)
		clockOut = 40;
	else if (i == 4)
		clockOut = 140;
	else if (i == 5)
		clockOut = 72;
	else if (i == 6)
		clockOut = 64;
	else if (i == 7)
		clockOut = 60;
	else if (i == 8)
		clockOut = 0;
	else
		clockOut = 12;

	return clockOut;
}

int TillMat::GetRightValue(int i)
{
	int clockOut = 0;

	if (i == 0)
		clockOut = 128;
	else if (i == 1)
		clockOut = 248;
	else if (i == 2)
		clockOut = 68;
	else if (i == 3)
		clockOut = 80;
	else if (i == 4)
		clockOut = 56;
	else if (i == 5)
		clockOut = 17;
	else if (i == 6)
		clockOut = 1;
	else if (i == 7)
		clockOut = 216;
	else if (i == 8)
		clockOut = 0;
	else
		clockOut = 24;

	return clockOut;
}