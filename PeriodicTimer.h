#pragma once

class PeriodicTimer
{
public:
	
	void update(unsigned long microDiff);
	void start(unsigned long periodicTime);
	void stop();
	bool wasTrigged();

private:

	bool isRunning = false, bWasTrigged = false;
	unsigned long internalTime, periodicTime;
};