#pragma once

#include "Button.h"
#include "LiquidCrystal.h"
#include "TillMat.h"
#include "Diode.h"
#include "Trigger.h"
#include "LedControl.h"


class HardwareManager
{
public:

	HardwareManager();

	void setup();
	void update(unsigned long microDiff);

	LiquidCrystal lcd;
	TillMat tillMat;
	
	//Big Display
	LedControl ledControl;

	// huvud enhet
	Button buttonDisplay1;
	Button buttonDisplay2;
	Button buttonSend;
	Button buttonStart;
	Button buttonStopReset;
	Button buttonTrigger1;
	Button buttonTrigger2;
	Diode diodeTrigger1;
	Diode diodeTrigger2;
	Trigger triggerChannel1;
	Trigger triggerChannel2;
	Trigger triggerStart;

	// till�gs m�tare
	Button buttonTillMatMinus;
	Button buttonTillMatPlus;
	Button buttonTillMatSend1;
	Button buttonTillMatSend2;
};
