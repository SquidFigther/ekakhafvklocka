#pragma once

#include "Arduino.h"

class Clock
{
public:
	
	void start();
	void stop();
	void reset();

	void update(unsigned long microDiff);

	void addTime(unsigned long time);

	unsigned long getTime();
	unsigned int getTimeSec();
	unsigned int getTimeDec();

	boolean isRunning();

	// return tenth of a second
	unsigned long getFinalTime(unsigned long penaltyTime);

private:

	void maxTimeTrigged();

	boolean isTicking = false;
	boolean maxTimeReached = false;

	unsigned long time = 0;
};