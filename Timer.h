#pragma once

class Timer
{
public:

	void update(unsigned long microDiff);
	void start(unsigned long microSeconds);
	void reset();
	bool isDone();

	unsigned long getSecondsLeft();

private:
	unsigned long time;
};