#include "HardwareManager.h"

HardwareManager::HardwareManager() 
	: buttonDisplay1(pDisplay1)
	, buttonDisplay2(pDisplay2)
	, buttonSend(pSend)
	, buttonStart(pStart)
	, buttonStopReset(pStopResset)
	, buttonTrigger1(pTrigger1)
	, buttonTrigger2(pTrigger2)
	, buttonTillMatMinus(pMinus)
	, buttonTillMatPlus(pPlus)
	, buttonTillMatSend1(pSend1)
	, buttonTillMatSend2(pSend2)
	, diodeTrigger1(pTrigger1Diode, true)
	, diodeTrigger2(pTrigger2Diode, true)
	, lcd(pLCDRS, pLCDE, pLCDD0, pLCDD1, pLCDD2, pLCDD3, pLCDD4, pLCDD5, pLCDD6, pLCDD7)
	, triggerStart(pAPezC1, pAPotC1)
	, triggerChannel1(pAPezC2, pAPotC2)
	, triggerChannel2(pAPezC3, pAPotC3)
	, ledControl(pDin, pCLK, pCS, 1)
{
}

void HardwareManager::setup()
{
	analogReadResolution(12); //0-4095

	pinMode(pStart, INPUT);
	pinMode(pStopResset, INPUT);
	pinMode(pDisplay1, INPUT);
	pinMode(pDisplay2, INPUT);
	pinMode(pTrigger1, INPUT);
	pinMode(pTrigger2, INPUT);
	pinMode(pSend, INPUT);

	pinMode(pLCDRS, OUTPUT);
	pinMode(pLCDE, OUTPUT);
	pinMode(pLCDD0, OUTPUT);
	pinMode(pLCDD1, OUTPUT);
	pinMode(pLCDD2, OUTPUT);
	pinMode(pLCDD3, OUTPUT);
	pinMode(pLCDD4, OUTPUT);
	pinMode(pLCDD5, OUTPUT);
	pinMode(pLCDD6, OUTPUT);
	pinMode(pLCDD7, OUTPUT);

	pinMode(pRCLK, OUTPUT);
	pinMode(pSRCLK, OUTPUT);
	pinMode(pSER, OUTPUT);

	pinMode(pSend1, INPUT);
	pinMode(pSend2, INPUT);
	pinMode(pMinus, INPUT);
	pinMode(pPlus, INPUT);

	pinMode(pTrigger1Diode, OUTPUT);
	pinMode(pTrigger2Diode, OUTPUT);
	diodeTrigger1.turnOff();
	diodeTrigger2.turnOff();
	//TODO lysdioder

	pinMode(pDin, OUTPUT);
	pinMode(pCS, OUTPUT);
	pinMode(pCLK, OUTPUT);

	//allt annat b�rj g�ras efter att pinnarna init
	lcd.begin(cLCDCollums, cLCDRows);
	//beh�ver inte clearas g�rs det n�r vaje state b�rjar
}

void HardwareManager::update(unsigned long microDiff)
{
	buttonDisplay1.update(microDiff);
	buttonDisplay2.update(microDiff);
	buttonSend.update(microDiff);
	buttonStart.update(microDiff);
	buttonStopReset.update(microDiff);
	buttonTrigger1.update(microDiff);
	buttonTrigger2.update(microDiff);
	buttonTillMatMinus.update(microDiff);
	buttonTillMatPlus.update(microDiff);
	buttonTillMatSend1.update(microDiff);
	buttonTillMatSend2.update(microDiff);
	triggerStart.update(microDiff);
	triggerChannel1.update(microDiff);
	triggerChannel2.update(microDiff);
}