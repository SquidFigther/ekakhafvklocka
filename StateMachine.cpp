#include "StateMachine.h"
#include "IState.h"

void StateMachine::update(unsigned long microDiff)
{
	currentState->update(microDiff);
}

void StateMachine::switchState(IState *newState)
{
	currentState = newState;
	currentState->activate();
}