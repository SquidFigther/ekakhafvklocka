#include "BaseState.h"
#include "Config.h"
#include "HardwareManager.h"

extern HardwareManager hardwareManager;

void BaseState::activate()
{
	hardwareManager.lcd.clear();
	resetDisplayTimer();

	initDisplay();
	hardwareManager.ledControl.clearDisplay(0);
}

void BaseState::update(unsigned long microDiff)
{
	TimeToUpdateDisplay -= microDiff;
	
	if (TimeToUpdateDisplay < 1)
	{
		updateDisplay();
		resetDisplayTimer();
	}
}

void BaseState::updateDisplay()
{
	hardwareManager.tillMat.update(tillMatState);
}

void BaseState::initDisplay()
{
	hardwareManager.ledControl.shutdown(0, false);
	hardwareManager.ledControl.setScanLimit(0, 4);
	hardwareManager.ledControl.setIntensity(0, 15);
}

void BaseState::resetDisplayTimer()
{
	TimeToUpdateDisplay += oneSecondInMicroSeconds / displayUpdateHz;
}