#include "Channel.h"
#include "HardwareManager.h"

extern HardwareManager hardwareManager;

const unsigned long ONE_MIN = oneSecondInMicroSeconds * 60;

Channel::Channel(int channel, TillMatState &tillMat)
: channel(channel)
, tillMatChannel(channel, tillMat)
, lcdPrinter(channel)
, isActive(false)
, useFirstClock(true)
{
}

void Channel::reset()
{
	changeState(Init);
}

void Channel::update(unsigned long microDiff)
{
	clock1.update(microDiff);
	clock2.update(microDiff);

	timer1.update(microDiff);
	timer2.update(microDiff);

	if (isActive && hardwareManager.buttonDisplay2.trigged()
		&& currentState != PenaltyInput && currentState != Done)
	{
		toggleClock();
	}

	if (isActive && hardwareManager.buttonSend.trigged())
	{
		Clock &clock = useFirstClock ? clock1 : clock2;

		unsigned long sec, dec;
		getTime(sec, dec);

		Serial.print(clock.getTimeSec());
		Serial.print(".");
		Serial.print(clock.getTimeDec());
		
		Serial.print(";");

		if (currentState == Done)
		{
			Serial.print(penaltyTime / 10);
			Serial.print(".");
			Serial.print(penaltyTime % 10);
		}
		else
		{
			Serial.print("0.0");
		}

		Serial.print(";");

		Serial.print(sec);
		Serial.print(".");
		Serial.println(dec);
	}

	switch (currentState)
	{
	case Init:

		if (isActive)
		{
			if (hardwareManager.buttonStart.trigged()
				|| hardwareManager.triggerStart.trigged())
			{
				changeState(Running);
			}

			if (hardwareManager.buttonStopReset.trigged())
			{
				changeState(Init);
			}
		}
		break;

	case Running:

		if (isActive)
		{
			if (hardwareManager.buttonStopReset.trigged())
			{
				clock1.stop();
				clock2.stop();
				timer1.start(ONE_MIN);
				timer2.start(ONE_MIN);

				changeState(PenaltyWait);
				break;
			}

			if ((hardwareManager.triggerChannel1.trigged()
				|| hardwareManager.triggerChannel2.trigged())
				&& clock1.getTimeSec() > 1)
			{
				clock1.stop();
				timer1.start(ONE_MIN);
				changeState(PenaltyWait);
			}

		}
		break;

	case PenaltyWait:

		if (isActive)
		{
			if (hardwareManager.buttonStopReset.trigged())
			{
				changeState(Init);
				break;
			}

			if ((hardwareManager.triggerChannel1.trigged()
				|| hardwareManager.triggerChannel2.trigged())
				&& clock2.isRunning())
			{
				clock2.stop();
				timer2.start(ONE_MIN);
			}
		}

		if (useFirstClock && timer1.isDone() && !clock1.isRunning() 
			|| !useFirstClock && timer2.isDone() && !clock2.isRunning())
		{
			timer1.reset();
			timer2.reset();

			if (useFirstClock)
			{
				clock1.stop();
				clock2.reset();
			}
			else
			{
				clock1.reset();
				clock2.stop();
			}

			changeState(PenaltyInput);
		}

		break;

	case PenaltyInput:

		if (isActive && hardwareManager.buttonStopReset.trigged())
		{
			if (tillMatChannel.isOwner(this))
				tillMatChannel.acquire(0);
			
			changeState(Init);
			break;
		}

		if (tillMatChannel.isOwner(this))
		{
			if (hardwareManager.buttonTillMatPlus.trigged())
			{
				if (penaltyTime < 99)
					++penaltyTime;
			}

			if (hardwareManager.buttonTillMatMinus.trigged())
			{
				if (penaltyTime > 0)
					--penaltyTime;
			}

			if (hardwareManager.buttonTillMatSend2.trigged())
			{
				penaltyTime = 0;
			}

			if (hardwareManager.buttonTillMatSend1.trigged())
			{
				tillMatChannel.acquire(0);
				changeState(Done);
			}
		}
		else if (tillMatChannel.canAcquire())
		{
			tillMatChannel.acquire(this);
		}

		break;

	case Done:

		if (isActive && hardwareManager.buttonStopReset.trigged())
		{
			changeState(Init);
			break;
		}

		break;
	}
}

void Channel::updateDisplay()
{
	updateTillMat();
	updateLCD();

	if (isActive)
	{
		updateBigDisplay();
	}
}

void Channel::getTime(unsigned long &secOut, unsigned long &decOut)
{
	Clock &clock = useFirstClock ? clock1 : clock2;
	bool usePenaltyTime = currentState == Done && penaltyTime > 0;

	if (usePenaltyTime)
	{
		unsigned int finalTime = clock.getFinalTime(penaltyTime);

		secOut = finalTime / 10;
		decOut = finalTime % 10;
	}
	else
	{
		secOut = clock.getTimeSec();
		decOut = clock.getTimeDec();
	}
}

void Channel::changeState(ChannelState newState)
{
	currentState = newState;

	switch (currentState)
	{
	case Init:

		clock1.reset();
		clock2.reset();

		timer1.reset();
		timer2.reset();

		penaltyTime = 0;
		useFirstClock = true;

		lcdPrinter.resetChannel();
		break;

	case Running:

		clock1.start();
		clock2.start();
		break;
	}

	tillMatChannel.setBuzzer(false);
}

void Channel::updateLCD()
{
	lcdPrinter.printLCDTime(clock1, clock2);

	if (currentState == PenaltyWait)
	{
		Timer &timer = useFirstClock ? timer1 : timer2;
		Clock &clock = useFirstClock ? clock1 : clock2;

		if (clock.isRunning())
			lcdPrinter.printTTime(60);
		else
			lcdPrinter.printTTime(timer.getSecondsLeft());
	}
	else if (currentState == PenaltyInput || currentState == Done)
	{
		lcdPrinter.printTTime(0);
	}

	if (currentState == Done)
	{
		lcdPrinter.printPTime(penaltyTime);

		Clock &clock = useFirstClock ? clock1 : clock2;
		
		boolean hadPenalty = penaltyTime > 0;
	
		unsigned long sec, dec;
		getTime(sec, dec);

		lcdPrinter.printFinalTime(sec, dec, hadPenalty);
	}
}

void Channel::updateBigDisplay()
{
	hardwareManager.ledControl.fixShit();

	if (!isActive) return;

	unsigned long clockSec;
	unsigned long clockDec;

	getTime(clockSec, clockDec);

	int sec10 = (clockSec - ((clockSec / 100) * 100)) / 10;
	int sec1 = (clockSec - ((clockSec / 10) * 10)) / 1;

	if (clockSec < 10)
	{
		hardwareManager.ledControl.setChar(0, 0, ' ', true);
	}
	else
	{
		hardwareManager.ledControl.setChar(0, 0, sec10, true);
	}

	hardwareManager.ledControl.setChar(0, 1, sec1, true);

	if (penaltyTime > 0 && currentState == Done)
	{
		hardwareManager.ledControl.setChar(0, 2, clockDec, true);
		hardwareManager.ledControl.setChar(0, 3, ' ', true);
		hardwareManager.ledControl.setChar(0, 4, ' ', true);
	}
	else
	{
		int dec1 = (clockDec - ((clockDec / 1000) * 1000)) / 100;
		int dec10 = (clockDec - ((clockDec / 100) * 100)) / 10;
		int dec100 = (clockDec - ((clockDec / 10) * 10)) / 1;

		hardwareManager.ledControl.setChar(0, 2, dec1, true);
		hardwareManager.ledControl.setChar(0, 3, dec10, true);
		hardwareManager.ledControl.setChar(0, 4, dec100, true);
	}
}

void Channel::updateTillMat()
{
	boolean displayOff = currentState == Init || currentState == Running || currentState == Done;

	tillMatChannel.setDiod(tillMatChannel.isOwner(this));

	if (!displayOff)
	{
		tillMatChannel.setOnStatus(true);

		if (currentState != PenaltyInput)
		{
			Timer &timer = useFirstClock ? timer1 : timer2;
			unsigned long time = timer.getSecondsLeft();
			tillMatChannel.setTime(time);

			boolean buzzerOn = 30 < time && time <= 33 || 0 < time && time <= 3;
			tillMatChannel.setBuzzer(buzzerOn);
		}
		else
		{
			tillMatChannel.setTime(penaltyTime);
			tillMatChannel.setBuzzer(false);
		}
	}
	else
	{
		tillMatChannel.setOnStatus(false);
	}
}
