#pragma once

#include "Arduino.h"
#include "Config.h"

class Button
{
public:
	Button(int pin);

	void update(unsigned long microDiff);

	boolean trigged();
	unsigned long getMicroRest();

private:
	const int pin;

	//om triggad p� positiv flank
	boolean isTrigged;
	boolean isTriggable;

	unsigned long time;
	unsigned long microRest;
};
