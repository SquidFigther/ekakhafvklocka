#pragma once

/*
C1:000.000= 000.000<
T:30 P:11 A 000.000<
C2:000.000= 000.000<
T:30 P:11 A 000.000<
*/

#include "TillMat.h"
#include "BaseState.h"
#include "Channel.h"

class ClockState : public BaseState
{
public:
	
	ClockState();

	virtual void activate();
	virtual void updateDisplay();
	virtual void update(unsigned long microDiff);
	void updateActiveChannelArrow();

private:

	/*
	void updateBigDisplay();

	void updateInput();

	void resetLCDDisplayChannel1();
	void resetLCDDisplayChannel2();

	void printLCDTime(int clock, int coll, int row);
	void printLCDFinalTime(int clock, int coll, int row);
	*/

	Channel ch1, ch2;
};