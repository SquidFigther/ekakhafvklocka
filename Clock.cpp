#include "Clock.h"
#include "Config.h"

void Clock::start()
{
	isTicking = true;
}

void Clock::stop()
{
	isTicking = false;
}

void Clock::reset()
{
	isTicking = false;
	maxTimeReached = false;
	time = 0;
}

void Clock::update(unsigned long microDiff)
{
	if (!maxTimeReached && isTicking)
	{
		time += microDiff;

		if (time >= oneSecondInMicroSeconds * 1000)
		{
			maxTimeTrigged();
		}
	}
}

void Clock::addTime(unsigned long time)
{
	if (time <= maxTimeClock)
	{
		Clock::time += time;
	}
	else
	{
		maxTimeTrigged();
	}
}

unsigned long Clock::getTime()
{
	return time;
}

unsigned int Clock::getTimeSec()
{
	return (time / oneSecondInMicroSeconds) % (oneSecondInMicroSeconds * 1000);
}

unsigned int Clock::getTimeDec()
{
	return (time % oneSecondInMicroSeconds) / 1000;
}

boolean Clock::isRunning()
{
	return isTicking;
}

unsigned long Clock::getFinalTime(unsigned long penaltyTime)
{
	unsigned long temp = (time / (oneSecondInMicroSeconds / 100));
	unsigned long rest = temp % 10;

	unsigned long roundedTime = time / (oneSecondInMicroSeconds / 10);

	roundedTime += penaltyTime;

	if (rest > 4)
		++roundedTime;

	unsigned long maxTime = maxTimeClock / 100000;
	
	if (roundedTime > maxTime) 
		return maxTime;

	return roundedTime;
}

void Clock::maxTimeTrigged()
{
	time = maxTimeClock;
	maxTimeReached = true;
	stop();
}
