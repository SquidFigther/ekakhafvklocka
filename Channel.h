#pragma once
#include "Arduino.h"
#include "Clock.h"
#include "ChannelLCDPrinter.h"
#include "Timer.h"
#include "TillMatChannel.h"

enum ChannelState { Init, Running, PenaltyWait, PenaltyInput, Done };

class Channel
{
public:

	Channel(int channel, TillMatState &tillMat);

	void reset();

	void update(unsigned long microDiff);
	void updateDisplay();

	void getTime(unsigned long &secOut, unsigned long &decOut);

	boolean getIsActive() const { return isActive; }
	void setIsActive(boolean val) { isActive = val; }

	boolean getUseFirstClock() const { return useFirstClock; }

private:

	void changeState(ChannelState newState);

	void toggleClock() { useFirstClock = !useFirstClock; }

	void updateLCD();
	void updateBigDisplay();
	void updateTillMat();

private:

	int channel;

	Clock clock1, clock2;
	Timer timer1, timer2;

	unsigned int penaltyTime;

	boolean isActive;
	boolean useFirstClock;
	
	ChannelState currentState;

	ChannelLCDPrinter lcdPrinter;
	TillMatChannel tillMatChannel;
};