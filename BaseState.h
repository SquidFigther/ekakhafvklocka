#pragma once

#include "IState.h"
#include "HardwareManager.h"
#include "StateMachine.h"
#include "TillMat.h"

class BaseState : public IState
{
public:

	virtual void activate();
	virtual void update(unsigned long microDiff);
	void initDisplay();
	
	virtual void updateDisplay();

private:

	void resetDisplayTimer();

protected: 

	TillMatState tillMatState;

private:

	long TimeToUpdateDisplay;
};