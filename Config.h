#pragma once

//pins analoga
const int pAPotC1 = 0;
const int pAPotC2 = 1;
const int pAPotC3 = 2;
const int pAPezC1 = 5;
const int pAPezC2 = 4;
const int pAPezC3 = 3;

//pin digital knappar
const int pStart = 43;
const int pStopResset = 41;
const int pDisplay1 = 51;
const int pDisplay2 = 49;
const int pTrigger1 = 45;
const int pTrigger1Diode = 52;
const int pTrigger2 = 47;
const int pTrigger2Diode = 50;
const int pSend = 53;

//pin digital lcd
const int pLCDRS = 48;
const int pLCDE = 32;
const int pLCDD0 = 33;
const int pLCDD1 = 35;
const int pLCDD2 = 31;
const int pLCDD3 = 29;
const int pLCDD4 = 27;
const int pLCDD5 = 25;
const int pLCDD6 = 23;
const int pLCDD7 = 28;

//pin digital till�gg
const int pRCLK = 26;
const int pSRCLK= 22;
const int pSER = 24;
const int pSend1 = 36;
const int pSend2 = 38;
const int pMinus = 40;
const int pPlus = 42;

//pin digital big display
const int pDin = 2;
const int pCS = 7;
const int pCLK = 6;

//constanter h�rdvara
const int cLCDCollums = 20;
const int cLCDRows = 4;

//constanter program
const unsigned long oneSecondInMicroSeconds = 1000000;
const unsigned long displayUpdateHz = 15;
const unsigned long buttonsFilterTime = 22000;
const unsigned long triggersFilterTime = oneSecondInMicroSeconds;
const unsigned int introTimeInSec = 5;

//�ndra inte denna om inte hela layouten f�r displayen �ndras
const unsigned int maxTimeClock = 999999000;