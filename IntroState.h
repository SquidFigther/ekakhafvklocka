#pragma once

#include "BaseState.h"
#include "Timer.h"

class IntroState : public BaseState
{
public:
	
	virtual void activate();
	virtual void updateDisplay();
	virtual void update(unsigned long microDiff);

private:

	Timer timer;
};