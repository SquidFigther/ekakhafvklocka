#include "Timer.h"
#include "Config.h"

void Timer::update(unsigned long microDiff)
{
	if (microDiff > time)
	{
		time = 0;
	}
	else
	{
		time -= microDiff;
	}
}

void Timer::start(unsigned long microSeconds)
{
	time = microSeconds > 0 ? microSeconds : 0;
}

void Timer::reset()
{
	time = 0;
}

bool Timer::isDone()
{
	return time < 1;
}

unsigned long Timer::getSecondsLeft()
{
	return time / oneSecondInMicroSeconds;
}
