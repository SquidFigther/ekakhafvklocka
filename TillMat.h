#pragma once

#include "Arduino.h"
#include "Config.h"

const int OFF_LEFT = 254;
const int OFF_RIGHT = 253;

struct TillMatState
{
	TillMatState();

	int time1;
	int time2;
	boolean diod1;
	boolean diod2;
	boolean buzzer;
	boolean rightDisplayOff;
	boolean leftDisplayOff;
};

class TillMat
{
public:

	void update(const TillMatState & updateState);

private:
	
	int GetLeftValue(int i);
	int GetRightValue(int i);
};
