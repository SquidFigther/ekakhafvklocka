/*
H�fvklocka


*/

#include "Arduino.h"
#include "Config.h"
#include "HardwareManager.h"
#include "StateMachine.h"
#include "IntroState.h"
#include "ClockState.h"

HardwareManager hardwareManager;
StateMachine stateMachine;

//states
IntroState introState;
ClockState clockState;

unsigned long previousTime;
unsigned long micro;
unsigned long microDiff;

void setup()
{
	Serial.begin(115200);

	hardwareManager.setup();
	stateMachine.switchState(&introState);

	Serial.println("Ekak hafvklocka started");
}

void loop()
{
	//tar snapshot av tiden f�r denna loopen
	takeTimeSnapshot();

	hardwareManager.update(microDiff);
	stateMachine.update(microDiff);
}

void takeTimeSnapshot()
{
	micro = micros();
	if (micro > previousTime)
		microDiff = micro - previousTime;
	else
		microDiff = 0xffffffff - previousTime + micro;
	previousTime = micro;
}