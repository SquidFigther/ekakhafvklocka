#include "PeriodicTimer.h"

void PeriodicTimer::update(unsigned long microDiff)
{
	if (!isRunning) return;

	if (microDiff > internalTime)
	{
		unsigned long overshoot = microDiff - internalTime;
		internalTime = periodicTime - overshoot;
		
		bWasTrigged = true;
	}
	else
	{
		internalTime -= microDiff;
		bWasTrigged = false;
	}
}

void PeriodicTimer::start(unsigned long periodicTime)
{
	if (periodicTime > 0)
	{
		PeriodicTimer::periodicTime = periodicTime;
		internalTime = periodicTime;
		isRunning = true;
	}
	else
	{
		isRunning = false;
	}

	bWasTrigged = false;
}

void PeriodicTimer::stop()
{
	isRunning = false;
	bWasTrigged = false;
}

bool PeriodicTimer::wasTrigged()
{
	return bWasTrigged;
}