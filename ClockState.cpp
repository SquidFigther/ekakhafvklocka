#include "ClockState.h"
#include "HardwareManager.h"

extern HardwareManager hardwareManager;

ClockState::ClockState()
: ch1(0, tillMatState)
, ch2(1, tillMatState)
{
}

void ClockState::activate()
{
	BaseState::activate();

	tillMatState.buzzer = false;
	tillMatState.diod1 = false;
	tillMatState.diod2 = false;
	tillMatState.time1 = 0;
	tillMatState.time2 = 0;
	tillMatState.rightDisplayOff = true;
	tillMatState.leftDisplayOff = true;

	hardwareManager.tillMat.update(tillMatState);

	// setup triggers initially
	hardwareManager.triggerStart.setReading(true);
	
	hardwareManager.triggerChannel1.setReading(true);
	hardwareManager.triggerChannel2.setReading(false);

	hardwareManager.diodeTrigger1.turnOn();
	hardwareManager.diodeTrigger2.turnOff();

	ch1.setIsActive(true);
	ch1.reset();
	ch2.reset();
}

void ClockState::updateDisplay()
{
	ch1.updateDisplay();
	ch2.updateDisplay();

	updateActiveChannelArrow();

	BaseState::updateDisplay();
}

void ClockState::update(unsigned long microDiff)
{
	if (hardwareManager.buttonDisplay1.trigged())
	{
		// toggle active channel
		boolean isCh1ACtive = ch1.getIsActive();

		ch1.setIsActive(!isCh1ACtive);
		ch2.setIsActive(isCh1ACtive);
	}

	if (hardwareManager.buttonTrigger1.trigged())
	{
		boolean newIsReading = !hardwareManager.triggerChannel1.Reading();
		hardwareManager.triggerChannel1.setReading(newIsReading);
		
		if (newIsReading)
			hardwareManager.diodeTrigger1.turnOn();
		else
			hardwareManager.diodeTrigger1.turnOff();
	}

	if (hardwareManager.buttonTrigger2.trigged())
	{
		boolean newIsReading = !hardwareManager.triggerChannel2.Reading();
		hardwareManager.triggerChannel2.setReading(newIsReading);

		if (newIsReading)
			hardwareManager.diodeTrigger2.turnOn();
		else
			hardwareManager.diodeTrigger2.turnOff();
	}

	ch1.update(microDiff);
	ch2.update(microDiff);

	BaseState::update(microDiff);
}

void ClockState::updateActiveChannelArrow()
{
	const char *row1 = ch1.getIsActive() ? 
		(ch1.getUseFirstClock() ? "<" : " ") 
		: (ch1.getUseFirstClock() ? "-" : " ");

	const char *row2 = ch1.getIsActive() ?
		(!ch1.getUseFirstClock() ? "<" : " ")
		: (!ch1.getUseFirstClock() ? "-" : " ");
	
	const char *row3 = ch2.getIsActive() ?
		(ch2.getUseFirstClock() ? "<" : " ")
		: (ch2.getUseFirstClock() ? "-" : " ");
	
	const char *row4 = ch2.getIsActive() ?
		(!ch2.getUseFirstClock() ? "<" : " ")
		: (!ch2.getUseFirstClock() ? "-" : " ");

	hardwareManager.lcd.setCursor(19, 0);
	hardwareManager.lcd.print(row1);
	hardwareManager.lcd.setCursor(19, 1);
	hardwareManager.lcd.print(row2);
	hardwareManager.lcd.setCursor(19, 2);
	hardwareManager.lcd.print(row3);
	hardwareManager.lcd.setCursor(19, 3);
	hardwareManager.lcd.print(row4);
}

/*
void ClockState::updateDisplay()
{
	printLCDTime(1, 12, 0);
	printLCDTime(2, 12, 1);
	printLCDTime(3, 12, 2);
	printLCDTime(4, 12, 3);

	updateBigDisplay();

	BaseState::updateDisplay();
}

void ClockState::update(unsigned long microDiff)
{
	//update input f�rst
	updateInput();

	CM.update(microDiff);

	//initDisplay();
	//hardwareManager.ledControl.clearDisplay(0);
	//updateBigDisplay();

	BaseState::update(microDiff);
}

void ClockState::updateBigDisplay()
{
	int clockIndex;

	int sec10;
	int sec1;
	int dec100;
	int dec10;
	int dec1;

	if (ch1Active)
	{
		if (chTime1Active)
			clockIndex = 1;
		else
			clockIndex = 2;
	}
	else
	{
		if (chTime1Active)
			clockIndex = 3;
		else
			clockIndex = 4;
	}

	sec10 = (CM.getClockSec(clockIndex, false) - ((CM.getClockSec(clockIndex, false) / 100) * 100)) / 10;
	sec1 = (CM.getClockSec(clockIndex, false) - ((CM.getClockSec(clockIndex, false) / 10) * 10)) / 1;
	dec100 = (CM.getClockDec(clockIndex, false) - ((CM.getClockDec(clockIndex, false) / 1000) * 1000)) / 100;
	dec10 = (CM.getClockDec(clockIndex, false) - ((CM.getClockDec(clockIndex, false) / 100) * 100)) / 10;
	dec1 = (CM.getClockDec(clockIndex, false) - ((CM.getClockDec(clockIndex, false) / 10) * 10)) / 1;

	if (CM.getClockSec(clockIndex, false) < 10)
	{
		hardwareManager.ledControl.setChar(0, 0, ' ', true);
	}
	else
	{
		hardwareManager.ledControl.setChar(0, 0, sec10, true);
	}
	hardwareManager.ledControl.setChar(0, 1, sec1, true);
	hardwareManager.ledControl.setChar(0, 2, dec100, true);
	hardwareManager.ledControl.setChar(0, 3, dec10, true);
	hardwareManager.ledControl.setChar(0, 4, dec1, true);
}

void ClockState::updateInput()
{
	// TODO: remove
	// rest display
	if (hardwareManager.buttonSend.trigged())
	{
		hardwareManager.ledControl.shutdown(0, false);
		hardwareManager.ledControl.setScanLimit(0, 4);
		hardwareManager.ledControl.clearDisplay(0);
		hardwareManager.ledControl.setIntensity(0, 8);
		delay(2000);
	}

	// triggers
	if (hardwareManager.buttonTrigger1.trigged())
	{
		hardwareManager.triggerChannel1.setReading(!hardwareManager.triggerChannel1.Reading());
		if (hardwareManager.triggerChannel1.Reading())
			hardwareManager.diodeTrigger1.turnOn();
		else
			hardwareManager.diodeTrigger1.turnOff();
	}

	if (hardwareManager.buttonTrigger2.trigged())
	{
		hardwareManager.triggerChannel2.setReading(!hardwareManager.triggerChannel2.Reading());
		if (hardwareManager.triggerChannel2.Reading())
			hardwareManager.diodeTrigger2.turnOn();
		else
			hardwareManager.diodeTrigger2.turnOff();
	}

	// start
	if (hardwareManager.triggerStart.trigged() || hardwareManager.buttonStart.trigged())
	{
		if (ch1Active)
		{
			CM.startCH1();
		}
		else
		{
			CM.startCH2();
		}
	}

	//stop resset
	if (hardwareManager.triggerChannel1.trigged() ||
		hardwareManager.triggerChannel2.trigged())
	{
		if (ch1Active)
		{
			CM.stopCH1(false);
		}
		else
		{
			CM.stopCH2(false);
		}
	}

	if (hardwareManager.buttonStopReset.trigged())
	{
		if (ch1Active)
		{
			if (CM.resetCH1())
				resetLCDDisplayChannel1();
		}
		else
		{
			if (CM.resetCH2())
				resetLCDDisplayChannel2();
		}
	}

	//change display (h�r ska in mer)
	if (hardwareManager.buttonDisplay1.trigged())
	{
		ch1Active = !ch1Active;
	}
	if (hardwareManager.buttonDisplay2.trigged())
	{
		chTime1Active = !chTime1Active;
	}

	if (hardwareManager.buttonDisplay1.trigged() || hardwareManager.buttonDisplay2.trigged())
	{
		if (ch1Active)
		{
			if (chTime1Active)
			{
				hardwareManager.lcd.setCursor(19, 0);
				hardwareManager.lcd.print("<");
				hardwareManager.lcd.setCursor(19, 1);
				hardwareManager.lcd.print(" ");
				hardwareManager.lcd.setCursor(19, 2);
				hardwareManager.lcd.print(" ");
				hardwareManager.lcd.setCursor(19, 3);
				hardwareManager.lcd.print(" ");
			}
			else
			{
				hardwareManager.lcd.setCursor(19, 0);
				hardwareManager.lcd.print(" ");
				hardwareManager.lcd.setCursor(19, 1);
				hardwareManager.lcd.print("<");
				hardwareManager.lcd.setCursor(19, 2);
				hardwareManager.lcd.print(" ");
				hardwareManager.lcd.setCursor(19, 3);
				hardwareManager.lcd.print(" ");
			}
		}
		else
		{
			if (chTime1Active)
			{
				hardwareManager.lcd.setCursor(19, 0);
				hardwareManager.lcd.print(" ");
				hardwareManager.lcd.setCursor(19, 1);
				hardwareManager.lcd.print(" ");
				hardwareManager.lcd.setCursor(19, 2);
				hardwareManager.lcd.print("<");
				hardwareManager.lcd.setCursor(19, 3);
				hardwareManager.lcd.print(" ");
			}
			else
			{
				hardwareManager.lcd.setCursor(19, 0);
				hardwareManager.lcd.print(" ");
				hardwareManager.lcd.setCursor(19, 1);
				hardwareManager.lcd.print(" ");
				hardwareManager.lcd.setCursor(19, 2);
				hardwareManager.lcd.print(" ");
				hardwareManager.lcd.setCursor(19, 3);
				hardwareManager.lcd.print("<");
			}
		}
	}
}

void ClockState::resetLCDDisplayChannel1()
{
	hardwareManager.lcd.setCursor(0, 0);
	hardwareManager.lcd.print("C1:  wait =   0.000");
	hardwareManager.lcd.setCursor(0, 1);
	hardwareManager.lcd.print("T:30 P:       0.000");
}

void ClockState::resetLCDDisplayChannel2()
{
	hardwareManager.lcd.setCursor(0, 2);
	hardwareManager.lcd.print("C2:  wait =   0.000");
	hardwareManager.lcd.setCursor(0, 3);
	hardwareManager.lcd.print("T:30 P:       0.000");
}

void ClockState::printLCDTime(int clock, int coll, int row)
{
	if (CM.getClockSec(clock, false) > 99)
		hardwareManager.lcd.setCursor(coll, row);
	else if (CM.getClockSec(clock, false) > 9)
		hardwareManager.lcd.setCursor(coll + 1, row);
	else
		hardwareManager.lcd.setCursor(coll + 2, row);

	hardwareManager.lcd.print(CM.getClockSec(clock, false));

	hardwareManager.lcd.setCursor(coll + 4, row);
	hardwareManager.lcd.print(CM.getClockDec(clock, false));
}

void ClockState::printLCDFinalTime(int clock, int coll, int row)
{
	if (CM.getClockSec(clock, true) > 99)
		hardwareManager.lcd.setCursor(coll, row);
	else if (CM.getClockSec(clock, true) > 9)
		hardwareManager.lcd.setCursor(coll + 1, row);
	else
		hardwareManager.lcd.setCursor(coll + 2, row);

	hardwareManager.lcd.print(CM.getClockSec(clock, true));

	hardwareManager.lcd.setCursor(coll + 4, row);
	hardwareManager.lcd.print(CM.getClockDec(clock, true));
}*/

