#include "Button.h"

Button::Button(int pin) 
	: pin(pin)
	, isTrigged(false)
	, isTriggable(true)
	, time(0)
	, microRest(0)
{
}

void Button::update(unsigned long microDiff)
{
	boolean isDown = digitalRead(pin);
	isTrigged = false;

	if (isDown)
	{
		time += microDiff;

		if (time > buttonsFilterTime &&
			isTriggable)
		{
			isTrigged = true;
			isTriggable = false;
			microRest = time - buttonsFilterTime;
			time = 0;
		}
	}
	else
	{
		isTriggable = true;
		time = 0;
	}
}

boolean Button::trigged()
{
	return isTrigged;
}

unsigned long Button::getMicroRest()
{
	return microRest;
}
