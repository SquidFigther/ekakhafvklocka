#include "TillMatChannel.h"
#include "TillMat.h"

int TillMatChannel::buzzerCount = 0;
class Channel *TillMatChannel::currentChannel = 0;

TillMatChannel::TillMatChannel(int channel, class TillMatState &tillMat)
: channel(channel)
, tillMat(tillMat)
{
}

bool TillMatChannel::isOwner(class Channel *channel)
{
	return currentChannel == channel;
}

void TillMatChannel::acquire(class Channel *channel)
{
	currentChannel = channel;
}

bool TillMatChannel::canAcquire()
{
	return !currentChannel;
}

void TillMatChannel::setTime(int time)
{
	if (channel == 0)
		tillMat.time1 = time;
	else
		tillMat.time2 = time;
}

void TillMatChannel::setBuzzer(boolean shouldBeOn)
{
	if (shouldBeOn != isBuzzerOn)
	{
		if (shouldBeOn)
		{
			++buzzerCount;
		}
		else
		{
			--buzzerCount;
		}

		isBuzzerOn = shouldBeOn;
		tillMat.buzzer = buzzerCount > 0;
	}
}

void TillMatChannel::setOnStatus(boolean isOn)
{
	if (channel == 0)
		tillMat.leftDisplayOff = !isOn;
	else
		tillMat.rightDisplayOff = !isOn;
}

void TillMatChannel::setDiod(boolean isOn)
{
	if (channel == 0)
		tillMat.diod1 = isOn;
	else
		tillMat.diod2 = isOn;
}