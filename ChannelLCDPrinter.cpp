#include "ChannelLCDPrinter.h"
#include "Clock.h"
#include "HardwareManager.h"

extern HardwareManager hardwareManager;

static const int COLL = 12;

ChannelLCDPrinter::ChannelLCDPrinter(int channel)
: channel(channel)
{
}

void ChannelLCDPrinter::resetChannel()
{
	const char *row1 = channel == 0 ? "C1:  wait =   0.000" : "C2:  wait =   0.000";
 
	hardwareManager.lcd.setCursor(0, channel * 2);
	hardwareManager.lcd.print(row1);
	hardwareManager.lcd.setCursor(0, channel * 2 + 1);
	hardwareManager.lcd.print("T:60 P:       0.000");
}

void ChannelLCDPrinter::printLCDTime(Clock &clock1, Clock &clock2)
{
	printRow(channel * 2, clock1);
	printRow(channel * 2 + 1, clock2);
}

void ChannelLCDPrinter::printTTime(unsigned long time)
{
	hardwareManager.lcd.setCursor(2, channel * 2 + 1);

	if (time >= 10)
	{
		hardwareManager.lcd.print(time);
	}
	else
	{
		hardwareManager.lcd.print(" ");
		hardwareManager.lcd.print(time);
	}
}

void ChannelLCDPrinter::printPTime(unsigned int penaltyTime)
{
	hardwareManager.lcd.setCursor(7, channel * 2 + 1);
	
	unsigned int timeSec = penaltyTime / 10;
	unsigned int timeRest = penaltyTime % 10;

	hardwareManager.lcd.print(timeSec);
	hardwareManager.lcd.print(".");
	hardwareManager.lcd.print(timeRest);
}

void ChannelLCDPrinter::printFinalTime(unsigned long timeSec, unsigned long timeRest, boolean hadPenalty)
{
	hardwareManager.lcd.setCursor(3, channel * 2);

	if (timeSec < 10)
		hardwareManager.lcd.print("  ");
	else if (timeSec < 100)
		hardwareManager.lcd.print(" ");

	hardwareManager.lcd.print(timeSec);
	hardwareManager.lcd.print(".");

	if (hadPenalty)
	{
		hardwareManager.lcd.print(timeRest);

		hardwareManager.lcd.print("  ");
	}
	else
	{
		if (timeRest < 10)
			hardwareManager.lcd.print("00");
		else if (timeRest < 100)
			hardwareManager.lcd.print("0");

		hardwareManager.lcd.print(timeRest);
	}
}

void ChannelLCDPrinter::printRow(int row, Clock &clock)
{
	unsigned int timeSec = clock.getTimeSec();
	unsigned int timeDec = clock.getTimeDec();

	hardwareManager.lcd.setCursor(COLL, row);

	if(timeSec < 10)
		hardwareManager.lcd.print("  ");
	else if (timeSec < 100)
		hardwareManager.lcd.print(" ");

	hardwareManager.lcd.print(timeSec);
	hardwareManager.lcd.print(".");

	if (timeDec < 10)
		hardwareManager.lcd.print("00");
	else if (timeDec < 100)
		hardwareManager.lcd.print("0");

	hardwareManager.lcd.print(timeDec);
}
